![UCV logo](https://www.ucv.edu.pe/wp-content/uploads/2020/05/ucv-logo.png)

### FACULTAD DE INGENIERÍA Y ARQUITECTURA
### ESCUELA PROFESIONAL INGENIERÍA DE SISTEMAS

## INFORME ACADÉMICO

#### “SISTEMA DE MESA DE AYUDA O SISTEMA DE TRÁMITE DOCUMENTARIO”

#### AUTORES:

INTEGRANTES:| ORCID| PORCENTAJE DE TRABAJO
---|---|---
Bejarano Briones Brus Brandon|0000-0002-6492-5796 | 100%
Bernal Loayza Luis Neil| 0000-0002-6990-6113| 100%
Garcia Roman Sebastian Giovanny| 0000-0003-2035-2823| 100%
Rojas Mendoza Milton| 0000-0002-2045-1843| 100%
Torres Cabracancha Erick Andress| 0000-0003-1620-2435| 100%
Vasquez Atanacio Fabrizio Andree| 0000-0003-4015-6873| 10%

#### ASESOR:
* Richard Leonardo Berrocal Navarro (0000-0001-87183150)

#### LIMA – PERÚ
#### 2022

***
## NIVEL DE PARTICIPACIÓN DE LOS INTEGRANTES

![participación](imagenes/particip.jpg)

***


**1. RESUMEN EJECUTIVO**

El presente trabajo de investigación desea lograr la automatización de los trámites documentarios realizados en la Municipalidad Distrital de Santa Rosa, por tal motivo se programó la implementación de un Sistema Informático de Gestión de Trámite Documentario para la (MDSR) para optimizar el tiempo de atención de los expedientes logrando una mayor eficacia y eficiencia en el tiempo de respuesta a la localización de los mismos, logrando generar una mejor comunicación entre los trabajadores involucrados, siendo más fiable la respuesta y las decisiones de las autoridades de la misma. 





**2. INTRODUCCIÒN**

De acuerdo a lo publicado en el portal de la (Municipalidad Distrital de Santa Rosa - MDSR), es un organismo público ejecutor, encargado de planificar, organizar y ejecutar los planes estratégicos para las mejoras del distrito (Santa Rosa), así como los procesos de contrataciones de bienes, servicios, obras y consultorías a su cargo.

La MDSR cuenta con un total de diez (10) Gerencias, catorce (14) Sub gerencias, una (01) secretaria general, un (01) despacho de Alcaldía, un (01) órgano de gestión y un (01) órgano de control interno; los cuales uno de sus principales canales de comunicación es el uso de documentos físicos, lo que permite dar atención a las diversas solicitudes requeridas.

Las entidades externas a la MDSR, utilizan el mismo medio de comunicación, por lo que la mesa de partes central es la encargada de la recepción y derivación de estos documentos a las dependencias correspondientes.

El presente proyecto surgió ante la preocupación de esta entidad, debido a los retrasos en atención de sus dependencias, de las diferentes solicitudes recibidas mediante documentos físicos, así como la falta de control de dichos documentos. 

Es por ello, que se desarrolló este proyecto, el cual brinda la implementación de un Sistema de Trámite Documentario, que permita la automatización de sus procesos internos, con la finalidad de optimizar los procedimientos de elaboración, emisión y seguimiento de documentos.

Este proyecto se encuentra dentro del alcance de la ley N.º 27444, Ley del Procedimiento Administrativo General y ley N.º 27658, Ley Marco de Modernización de la Gestión del Estado, los cuales tienen como finalidad la eliminación de toda complejidad innecesaria en los procesos administrativos de las entidades del estado peruano, permitiendo la obtención de mayores niveles de eficiencia del aparato estatal, de manera que se logre una mejor atención a la ciudadanía.

**1. ESTUDIO DE FACTIBILIDAD**

Para cumplir con los objetivos trazados y llevar a cabo la implementación del Sistema de Trámite Documentario nos hemos apoyado en los siguientes aspectos:

**-Factibilidad Administrativa**

La administración se define como el proceso de delinear y conservar un ambiente en el que las personas trabajando en grupo, alcancen con eficiencia metas establecidas.

Esta se aplica a todo tipo de organizaciones, bien sean pequeñas o grandes, a las industrias manufactureras y a las de servicio.

Por este motivo, este proyecto es factible administrativamente, ya que se contará con el apoyo y autorización de la máxima autoridad de la Corporación.

**-Factibilidad Económica**

La evaluación económica pretende determinar cuál es el monto de los recursos económicos necesarios para la realización del proyecto, por lo que se ha determinado factible económicamente porque su implementación no resultara costosa.

Por ese motivo, este proyecto es factible económicamente debido al bajo costo de implementar esta propuesta.

**-Factibilidad Técnica**

El análisis de factibilidad técnica evalúa si el equipo y software están disponibles o en el caso del software, si puede desarrollarse y si se tienen las capacidades técnicas requeridas por cada alternativa del diseño que se esté considerando y por ese motivo este proyecto es factible a nivel técnico.

**1.1. Factibilidad operativa y técnica: La visión del sistema**

**2. MODELO DEL NEGOCIO** 

![Diagrama Erickson Penker](imagenes/erpenker.jpg)

**2.1. Modelo de Caso de Uso del Negocio**

En los modelos de CUN los principales actores son el Recepcionista y el Cliente

Procesos:

 _*CUN de Datos obligatorios del expediente:*_

 _Recepcionista:_

* Registrar clientes: El recepcionista registra el cliente.
* Registrar datos: El recepcionista registra los datos del cliente.
* Registrar pedido: El recepcionista registra el pedido del cliente. Examina el pedido para comprobar la factibilidad y poder aceptarlo.
* Consultar datos: El recepcionista consulta si ya tiene los datos del cliente en su base de datos.

* _Cliente:_

* Registrar datos: El cliente envía sus datos correspondientes.
* Registrar pedido: El cliente envía sus pedidos.

_*CUN de Conectarse a una red:*_

_Cliente:_

* Conectarse a internet: El cliente necesita conectarse a internet para lograr enviar su pedido. Antes de ese proceso verificar su internet.
* Enviar documento a tramitar: El cliente envía su pedido.

_Recepcionista:_

* Enviar documento a tramitar: El recepcionista recibe el pedido del cliente.

_*CUN de áreas actualizadas:*_

_Recepcionista:_

* Registro de area actualizada: Si el recepcionista obtiene los datos del cliente y recibe datos nuevos de este, los actualizará para esto ingresa al expediente del cliente y lo actualizará

**2.1.1. Lista de los Actores del Negocio**

Contribuyente/Cliente: Es el emisor, por lo cual hace el pedido correspondiente para lograr agilizar el trámite que quiere conseguir. Este actor tiene que enviar su pedido con sus datos correspondientes, como nombre, apellido, DNI, etc.
 
Recepcionista/Responsable de mesa: Es el receptor, por lo cual se encarga de recibir el pedido del cliente, también hace algunos procedimientos internos (registrar pedido, registrar cliente y datos, etc), al terminar los procedimientos internos lo deriva al encargado de la gerencia de atención al ciudadano, este último filtra el pedido y le da un número de pedido y expediente.

**2.1.2 Lista de casos de uso de negocio**

* Registrar cliente
* Registrar datos
* Conexión a red
* Actualizar registro 

**2.1.3. Diagrama de casos de negocio**

Un diagrama de caso de uso en el que el sistema que modela es el área de negocio del mundo real. Ofrece una visión general de los procesos y servicios (CUN) y las entidades que utilizan esos servicios o participan en su implementación.

* Diagrama de caso de negocio del proceso registrar cliente

![Image2](imagenes/registrar1.jpg)

* Diagrama de caso de negocio del proceso conexión a red

![Image3](imagenes/nectRed.jpg)

* Diagrama de caso de negocio del proceso registro de área actualizada

![Image4](imagenes/registro.jpg)

Un diagrama de caso de uso puede incluir varios casos de uso y las relaciones entre casos de uso y las personas, los grupos o los sistemas que interactúan para llevar a cabo el caso de uso.

**2.1.4. Especificaciones  de casos de uso de negocio**

Una especificación de caso de uso proporciona detalles textuales de un caso de uso. Se proporciona una descripción de ejemplo de una especificación de caso de uso. Puede reutilizar y modificar la descripción según se requiera en una especificación de caso de uso.

**-Nombre de caso de uso** Indica el nombre del caso de uso. Normalmente, el nombre expresa el resultado objetivo y observable del caso de uso, como por ejemplo "Retirar efectivo" en el caso de un cajero automático.

**-Breve descripción** Describe el rol y el objetivo del caso de uso.

**-Flujo de eventos** Describe  el comportamiento del sistema; no describe cómo el sistema funciona, los detalles de la presentación ni los detalles de la interfaz de usuario. Si se intercambia información, el caso de uso debe ser específico sobre lo que se ha transmitido.

**-Flujo básico** Describe el comportamiento ideal y principal del sistema.

**-Condiciones previas** Estado del sistema que debe estar presente antes del inicio del caso de uso.

**-Condiciones posteriores** Una lista de posibles estados del sistema.

**-Puntos de ampliación** Punto del flujo de eventos de caso de uso en el que se hace referencia a otro.

**-Requisitos especiales** Requisitos no funcionales que son específicos de un caso de uso pero que no se especifican en el texto del flujo de sucesos del caso de uso. Ejemplos :requisitos legales y reguladores; estándares de aplicación; atributos de calidad del sistema, incluidos la usabilidad, la fiabilidad, el rendimiento y la capacidad de soporte; sistemas operativos y entornos; requisitos de compatibilidad y limitaciones de diseño.

**-Flujos alternativos** Describe excepciones o desviaciones del flujo básico, como el modo en que el sistema se comporta cuando el actor especifica un ID de usuario incorrecto y la autenticación falla.

**2.2. Modelo de Análisis del Negocio**

![Modelo de Negocio](imagenes/2_2ModeloNegocio.jpg)

**2.2.1. Lista de trabajadores de negocio (TN)**

Son los que desarrollan el proceso de negocio. Se refiere a los roles, más no a los cargos.

* TN1. Recepcionista
* TN2. Asistente
* TN3. Responsable de Área

![Lista Trabajadores](imagenes/2_2_1ListaTrabajadores.jpg)

**2.2.2. Lista de entidades de negocio**



Todo lo que es papel o lo que pueda convertirse en papel. singular y sustantivo

* Tramite virtual

* Expediente

* Oficina

* Computadora

* Respuesta

* Formato

![Lista Negocio](imagenes/2_2_2ListaNegocio.jpg)


**2.2.3. Realización de Casos de Uso del Negocio**

**2.2.4. Diagrama de actividades**

![Image5](imagenes/Activi1.jpg)

![Image6](imagenes/Activi2.jpg)

**2.2.5. Realización de clases de dominio**

**2.3. Glosario de términos**

* MODELO DEL NEGOCIO: Es una herramienta previa al plan de negocio que te permitirá definir con claridad qué vas a ofrecer al mercado, cómo lo vas a hacer, a quién se lo vas a vender, cómo se lo vas a vender y de qué forma vas a generar ingresos.

* ACTORES: es cualquier individuo, grupo, entidad, organización, máquina o sistema de información externos; con los que el negocio interactúa.

* DIAGRAMA DE CASOS DE NEGOCIO: es un diagrama de caso de uso en el que el sistema que modela es el área de negocio del mundo real. Ofrece una visión general de los procesos y servicios (CUN) y las entidades que utilizan esos servicios o participan en su implementación.

* CASOS DEL USO DEL NEGOCIO: representa a un proceso de negocio, por lo que se corresponde con una secuencia de acciones que producen un resultado observable para ciertos actores del negocio.



---

3.1.2 Matriz de actividades y Requerimientos

![Matriz de Actividades](imagenes/matriz.jpeg)

3.2 Modelo de caso de uso

Para conseguir sus objetivos, una empresa organiza su actividad por medio de un conjunto de procesos de negocio. Cada uno de ellos se caracteriza por una colección de datos que son producidos y manipulados mediante un conjunto de tareas, en las que ciertos agentes (por ejemplo, trabajadores o departamentos) participan de acuerdo a un flujo de trabajo determinado. Además, estos procesos se hallan sujetos a un conjunto de reglas de negocio, que determinan las políticas y la estructura de la información de la empresa. Por tanto, la finalidad del modelado del negocio es describir cada proceso del negocio, especificando sus datos, actividades (o tareas), roles (o agentes) y reglas de negocio.

![Modelo de caso de uso](imagenes/modelo.jpg)

3.2.1 Lista de actores del Sistema

Un actor del negocio es cualquier individuo, grupo, entidad, organización, máquina o sistema de información externos; con los que el negocio interactúa. 

•	Recepcionista

•	Cliente

![lista](imagenes/lista1.jpg)


**3.2.2 Lista de casos de uso de negocio**

1.	ACTORES

•	Recepcionista

•	Cliente

•	Responsable de Área

2.	CASOS DE USO

•	CUS1.Registrar Datos

•	CUS2.Registrar Expediente

•	CUS3.Registrar Cliente

•	CUS4.Consultar Datos

•	CUS5.Enviar Tramite

•	CUS6.Consultar Cliente

•	CUS7.Consultar Expediente

•	CUS8.Cancelar Tramite

•	CUS9.Actualizar Datos

•	CUS10.Establecer Conexión

•	CUS11.Registrar Tramite

•	CUS12.Verificar Conexión

•	CUS13.Verificar Datos

•	CUS14.Verificar Expediente.

•	CUS15.Asignar Expediente

![CUS](imagenes/cus.jpg)

**3.2.3 Lista de casos de uso priorizado**

Estos son priorizados en el orden en que serán tratados en las iteraciones, se hace un ranking con los casos de uso identificados hasta el momento, de acuerdo al nivel de riesgo que implican, por ejemplo, tener que cambiar la arquitectura del sistema en fases posteriores es un riesgo que se quiere prevenir, no construir el sistema correcto es un riesgo que se quiere mitigar tempranamente, encontrando los requerimientos verdaderos. El proceso de selección también es riesgoso, pues se pueden cometer equivocaciones al rankear los casos de uso.

**3.2.4 Especificaciones de requerimientos de software**

R1: Permitir la autenticación de los usuarios. 

R2: Permitir la gestión (crear, modificar, eliminar) de usuarios, clientes y socios.

R3: Aperturas libretas de ahorro. 

R4: Realizar operaciones de depósitos(ahorros). R5: Realizar operaciones de créditos.

**Requisitos comunes de los interfaces**

•	Interfaces de usuario 

•	Interfaces de hardware 

•	Interfaces de software 

•	Interfaces de comunicación 

**Requisitos funcionales**

INTRODUCCION. El sistema debe permitir el ingreso del nombre y password del usuario para realizar las diferentes funciones que tendrá cada uno.

ENTRADAS Cedula, Nombres, Apellidos, Contraseña, Tipo de Usuario (Técnico, Cajero, Gerente).

PROCESOS El sistema pedirá la correspondiente identificación como administrador. Nos ubicamos en la parte de Administrar del menú principal y escogemos

SALIDAS Las salidas van dirigidas a: Administrador (Gerente). Mensaje de error en el caso de no haber llenado algún caso.

•	Requisito funcional 1

•	Requisito funcional 2

•	Requisito funcional 3

•	Requisito funcional n

**Requisitos no funcionales**

•	Requisitos de rendimiento

•	Seguridad

•	Fiabilidad

•	Disponibilida

•	Mantenibilidad

•	Portabilidad

**3.2.6 Especificaciones de casos de uso**

![referencial](imagenes/referencial.jpg)

imagen referencial

**Especificaciones del diagrama de casos de uso**

**CASO 1:**

Caso de uso:| Ingeso a la pagina
--------------- | ------------------
Actores:   | Recepcionista, cliente, empleado
Resumen:   | Existirá una página de inicio de sesión, la cual contiene las casillas para ingresar el nombre de usuario y contraseña.
Tipo:      | primaria y esencial 
.

**CASO 2:**

Caso de uso:| Inicio de consultar datos
--------------- | ------------------
Actores:   | Recepcionista, cliente, empleado
Resumen:   | el receptor ingresara los datos del cliente para consultar si está registrado en el aplicativo  
Tipo:      | primaria y esencial
.

**CASO 3:**

Caso de uso:| Registrar cliente
--------------- | ------------------
Actores:   | Recepcionista, cliente, empleado
Resumen:   | el receptor ingresara los datos del cliente para  registrar  al cliente y adjuntarlo  la base de datos 
Tipo:      | primaria
.

**CASO 4:**

Caso de uso:| Registrar pedidos
--------------- | ------------------
Actores:   | Recepcionista, cliente
Resumen:   | Se registra los datos del cliente , pedido, trámite que desea realizar 
Tipo:      | Esencial 
.

**CASO 5:**

Caso de uso:| Consultar pedido
--------------- | ------------------
Actores:   | Recepcionista, empleado
Resumen:   | El  recepcionista en este caso va verificar si el producto tiene en stock, de los contrario la consulta será 0
Tipo:      | Esencial 
.

**CASO 6:**

Caso de uso:| Actualizar pedido 
--------------- | ------------------
Actores:   | Recepcionista
Resumen:   | Al momento de realizar la consulta y tramitar el pedido se realiza una actualización de stock y se verifica las cantidades exactas  
Tipo:      | Esencial 
.

